# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'W:\DEPARTMENTS\DEV\deepakm\dev\shotgun_apps\alembic_exporter\resources\dialog_widget.ui'
#
# Created: Fri Nov 02 15:58:54 2018
#      by: pyside-uic 0.2.15 running on PySide 1.2.4
#
# WARNING! All changes made in this file will be lost!

from tank.platform.qt import QtCore, QtGui

class Ui_AlembicExporterDialog(object):
    def setupUi(self, AlembicExporterDialog):
        AlembicExporterDialog.setObjectName("AlembicExporterDialog")
        AlembicExporterDialog.resize(350, 697)
        sizePolicy = QtGui.QSizePolicy(QtGui.QSizePolicy.Preferred, QtGui.QSizePolicy.Preferred)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(AlembicExporterDialog.sizePolicy().hasHeightForWidth())
        AlembicExporterDialog.setSizePolicy(sizePolicy)
        AlembicExporterDialog.setMinimumSize(QtCore.QSize(350, 0))
        AlembicExporterDialog.setMaximumSize(QtCore.QSize(16777215, 16777215))
        self.verticalLayout = QtGui.QVBoxLayout(AlembicExporterDialog)
        self.verticalLayout.setContentsMargins(5, 5, 5, 5)
        self.verticalLayout.setObjectName("verticalLayout")
        self.verticalLayout_2 = QtGui.QVBoxLayout()
        self.verticalLayout_2.setObjectName("verticalLayout_2")
        self.label = QtGui.QLabel(AlembicExporterDialog)
        self.label.setObjectName("label")
        self.verticalLayout_2.addWidget(self.label)
        self.seq_cmb = QtGui.QComboBox(AlembicExporterDialog)
        self.seq_cmb.setObjectName("seq_cmb")
        self.verticalLayout_2.addWidget(self.seq_cmb)
        self.label_2 = QtGui.QLabel(AlembicExporterDialog)
        self.label_2.setObjectName("label_2")
        self.verticalLayout_2.addWidget(self.label_2)
        self.cut_cmb = QtGui.QComboBox(AlembicExporterDialog)
        self.cut_cmb.setObjectName("cut_cmb")
        self.verticalLayout_2.addWidget(self.cut_cmb)
        self.label_3 = QtGui.QLabel(AlembicExporterDialog)
        self.label_3.setObjectName("label_3")
        self.verticalLayout_2.addWidget(self.label_3)
        self.shots_list_view = QtGui.QListView(AlembicExporterDialog)
        self.shots_list_view.setEditTriggers(QtGui.QAbstractItemView.NoEditTriggers)
        self.shots_list_view.setSelectionMode(QtGui.QAbstractItemView.MultiSelection)
        self.shots_list_view.setObjectName("shots_list_view")
        self.verticalLayout_2.addWidget(self.shots_list_view)
        self.ok_btn = QtGui.QPushButton(AlembicExporterDialog)
        self.ok_btn.setObjectName("ok_btn")
        self.verticalLayout_2.addWidget(self.ok_btn)
        self.verticalLayout.addLayout(self.verticalLayout_2)

        self.retranslateUi(AlembicExporterDialog)
        QtCore.QMetaObject.connectSlotsByName(AlembicExporterDialog)

    def retranslateUi(self, AlembicExporterDialog):
        AlembicExporterDialog.setWindowTitle(QtGui.QApplication.translate("AlembicExporterDialog", "Dialog", None, QtGui.QApplication.UnicodeUTF8))
        self.label.setText(QtGui.QApplication.translate("AlembicExporterDialog", "Sequence", None, QtGui.QApplication.UnicodeUTF8))
        self.label_2.setText(QtGui.QApplication.translate("AlembicExporterDialog", "Cut", None, QtGui.QApplication.UnicodeUTF8))
        self.label_3.setText(QtGui.QApplication.translate("AlembicExporterDialog", "Shots", None, QtGui.QApplication.UnicodeUTF8))
        self.ok_btn.setText(QtGui.QApplication.translate("AlembicExporterDialog", "Export Selected Shots", None, QtGui.QApplication.UnicodeUTF8))

