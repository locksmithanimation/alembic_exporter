import os
import sys

import maya.standalone
maya.standalone.initialize()
from maya import cmds

from sequence_tools import shot_manager
from sequence_tools import camera_membership

class AlembicExport(object):
    def __init__(self):
        self.export_root = ""
        self._exp_group = None
        self.exp_group = None
        self.exported_cam_group = None
        self.not_exported = None
        self.tasks = []

    @property
    def exp_group(self):
        if self._exp_group:
            return cmds.ls(self._exp_group, uuid=True, long=True)[0]
        else:
            return None

    @exp_group.setter
    def exp_group(self, value):
        if value:
            self._exp_group = cmds.ls(value, uuid=True)
        else:
            self._exp_group = None

    def do_prepare(self, maya_file_to_open, ext, shots, export_root, cams_str, start_str, end_str):
        ##########################################
        # Open the file and plugin #
        ##########################################
        for plugin in ["camera_perlin_shake_node.py", "AbcExport.mll", "AbcImport.mll"]:
            loaded = cmds.pluginInfo(plugin, q=True, loaded=True)
            if not loaded:
                cmds.loadPlugin(plugin, quiet=True)

        opened_file = cmds.file(maya_file_to_open,
                                f=True,
                                options="v=0",
                                ignoreVersion=True,
                                typ=ext,
                                o=True)

        self.export_root = export_root

        #
        ###############################################################
        # Import all refs if loaded and set locksmith versions        #
        # Modified to ignose nested references. Only import top level #
        ###############################################################

        # This is weird. If we just import all the references in standalone
        # it tends to crash. We unload then reload the reference and import
        # it, it seems to work.

        refs = []
        namespaces = []
        for ref in cmds.ls(references=True):
            try:
                if cmds.referenceQuery(ref, referenceNode=True, topReference=True) != ref:
                    continue
                namespace = cmds.referenceQuery(ref, namespace=True)
            except RuntimeError:
                continue
            # Deal with the weird and still slightly inexplicable situation where two
            # references have the same namespace
            if namespace in namespaces:
                cmds.file(cmds.referenceQuery(ref, filename=True), edit=True, namespace=namespace)
                namespace = cmds.referenceQuery(ref, namespace=True)
            namespaces.append(namespace)
            is_loaded = cmds.referenceQuery(ref, isLoaded=True)
            if is_loaded:
                cmds.file(unloadReference=ref)
                refs.append(ref)
        for ref in refs:
            cmds.file(loadReference=ref)
            rFile = cmds.referenceQuery(ref, f=True)
            nodes = cmds.referenceQuery(rFile, nodes=True, dagPath=True)

            if isinstance(nodes, list) and len(nodes) > 0:
                if cmds.nodeType(nodes[0]) == 'transform':
                    # get version from ref file
                    file_tokens = rFile.split('.')
                    if len(file_tokens) > 2:
                        ver_str = file_tokens[len(file_tokens) - 2]
                        if 'v' in ver_str:
                            ver_str = ver_str[1:]
                        ver = int(ver_str)
                        if not cmds.attributeQuery("LocksmithVersion", node=nodes[0], exists=True):
                            cmds.addAttr(nodes[0], longName="LocksmithVersion", at="short")
                        cmds.setAttr(nodes[0] + '.LocksmithVersion', ver)
            cmds.file(rFile, importReference=True)

        #
        ##############################################
        # Delete all lights and unwanted light cameras
        ##############################################
        lights = cmds.ls(type="light")
        for light in lights:
            try:
                nd = cmds.listRelatives(light, parent=True, fullPath=True)
                cmds.delete(nd)
            except Exception:
                pass

        #
        ##############################################################################
        # Rename the cameras, remove namespaces and make sure camera names are unique
        ##############################################################################
        # after namespace removal all the cameras will have same name if not accessed by full dag path
        cams = cmds.ls(type="camera")
        for cam in cams:
            splt = cam.split(":")
            cam = cmds.listRelatives(cam, parent=True, fullPath=False)
            if len(splt) > 1:
                name = splt[len(splt) - 2] + "_Camera"
            else:
                name = cam[0] + "_Camera"

            #cmds.select(cam)
            try:
                cmds.rename(cam, name)
            except Exception:
                pass

        # delete all namespace
        # but before that apply namespace as name to top group node
        # get all namespaces but only top level, non recursive
        namespaces = cmds.namespaceInfo(listOnlyNamespaces=True)
        for namespace in namespaces:
            # get all nodes with this namespace
            nodes = cmds.namespaceInfo(namespace, listNamespace=True, dagPath=True)
            if nodes and len(nodes) > 0:
                for this_node in nodes:
                    if (len(this_node.split("|")[-1].split(":")) == 2 and
                            cmds.objExists(this_node) and
                                cmds.nodeType(this_node) == 'transform'):
                        break
                else:
                    this_node = None
                # we only care if type transform
                # get parent of only first node and find its way upto to top parent node
                # then change top node name and delete namespace
                if this_node:
                    parent = cmds.listRelatives(this_node, parent=True, fullPath=True)
                    if parent:
                        parents = parent[0].split('|')
                        sel_node = ""
                        replace_node = ""
                        # get the first node in hierarchy from top level which has namespace
                        for node in parents:
                            if node:
                                splt = node.split(':')
                                if len(splt) > 1:
                                    sel_node += '|' + node
                                    replace_node = node
                                    break
                                else:
                                    sel_node += '|' + node

                        # node name to be renamed and value to be replaced with
                        if sel_node and replace_node:
                            name = ""
                            splt = replace_node.split(':')
                            for i in splt:
                                name += splt[0] + ':'
                            name = name[:len(name) - 1]
                            # rename the node
                            # for e.g.  donka:rig becomes donka
                            cmds.rename(sel_node, name)
                            # finally remove namespace
                            cmds.namespace(mergeNamespaceWithParent=True, removeNamespace=namespace)

        # after deleting namespace and renaming camera
        # might still end up with same camera name under groups
        # find such camera and rename them to avoid name conflicts
        cams = cmds.ls(type="camera")
        for cam in cams:
            splt = cam.split('|')
            if len(splt) > 1:
                # here... would mean dagpath is conflicting hence using '|' to represent name
                name = splt[0] + "_Camera_Temp"
                try:
                    cam = cmds.listRelatives(cam, parent=True, fullPath=True)
                    cmds.rename(cam, name)
                except Exception:
                    pass

        #
        #################################################################
        # Create export group, what's left outside try to move into MISC
        #################################################################
        objs = []
        scene_groups = []
        look_for = ["CHAR", "char", "ENV", "env", "PROP", "prop", "PROPS", "props", "MISC", "misc"]
        ignore = ["FX", "MGFX", "CAMERA"]
        for node in look_for:
            if cmds.objExists(node):
                if len(cmds.ls(node, long=True)) > 1:
                    nodes = cmds.ls(node, long=True)
                    for this_node in nodes:
                        if len(this_node.split("|")) == 2:
                            objs.append(this_node)
                        elif len(this_node.split("|")) == 3 and this_node.split("|")[1] not in look_for:
                            objs.append(this_node)
                            scene_group = cmds.listRelatives(this_node, parent=True, fullPath=True)[0]
                            if scene_group not in scene_groups:
                                scene_groups.append(scene_group)
                else:
                    objs.append(node)

        self.exp_group = cmds.group(objs, name="|TEMP", world=True)
        # check if misc exists
        misc = ""
        if cmds.objExists("|TEMP|misc"):
            misc = "|TEMP|misc"
        elif cmds.objExists("|TEMP|MISC"):
            misc = "|TEMP|MISC"

        if not misc:
            # create one
            cmds.group(em=True, name="MISC", parent=self.exp_group)
            misc = "|TEMP|MISC"

        # now add everything thats not in list to this - minus CAMERAS and LIGHTS groups
        scene_groups.append("")
        for scene_group in scene_groups:
            nodes = cmds.ls(scene_group + "|*")
            for node in nodes:
                if cmds.ls(node, long=True)[0] == self.exp_group:
                    continue
                # don't add to misc if group contains cameras
                cams_in_group = cmds.ls(node, type="camera", dag=1)
                if (cmds.nodeType(node) == "transform" and
                            node != self.exp_group and
                            node not in look_for and
                            len(cams_in_group) == 0):
                    childs = cmds.listRelatives(node, children=True)
                    if childs and len(childs) > 0:
                        node_only = node.split("|")[-1]
                        if (cmds.nodeType("{}|{}|{}".format(scene_group, node_only, childs[0])) == "mesh" or
                                cmds.nodeType("{}|{}|{}".format(scene_group, node_only, childs[0])) == "transform"):
                            cmds.parent(node, misc)

        # create a camera group under
        cmds.group(em=True, name="CAMERAS", parent=self.exp_group)
        # create a group of placing exported cameras
        # in that way we can keep the cameras dir clean - one camera per export

        #
        ###########################
        # Make names more sensible
        ###########################
        # for e.g. barney10 to barney
        top = cmds.listRelatives(self.exp_group, children=True, fullPath=True)
        for groups in top:
            objects = cmds.listRelatives(groups, children=True, fullPath=True)
            if objects:
                # look for first 2 chars to match against
                first_chars = ([x.split("|")[-1][0:2] for x in objects])
                # dup_chars are names beginning with these may not be unique and shouldn't be renamed
                dup_chars = []
                for i, j in enumerate(first_chars):
                    if i < (len(first_chars) - 1):
                        if j in first_chars[i + 1:]:
                            dup_chars.append(j)

                # now look for objects name that don't have this char as prefix, and rename them if digit found
                for obj in objects:
                    obj_only = obj.split("|")[-1]
                    if cmds.referenceQuery(obj, isNodeReferenced=True):
                        continue
                    str_len = len(obj_only)
                    if obj_only[0:2] not in dup_chars:
                        for i, j in enumerate(obj_only):
                            if j.isdigit():
                                str_len = i
                                if obj_only[i - 1] == '_':
                                    str_len -= 1
                                break
                    # if str_len holds something, rename it
                    cmds.rename("|{}|{}|{}".format(self.exp_group, groups.split("|")[-1], obj_only), obj_only[:str_len])
        #
        ####################
        # Get the task data
        ####################
        shot_splt = shots.split(',')
        cams = cams_str.split(',')
        starts = start_str.split(',')
        ends = end_str.split(',')
        for shot, cam, start, end in zip(shot_splt, cams, starts, ends):
            temp = []
            temp.append(shot)
            temp.append(cam)
            temp.append(start)
            temp.append(end)
            temp.append(os.path.join(self.export_root, shot, shot + ".abc"))
            self.tasks.append(temp)

        #
        #########################################
        # Save this file - for debug purpose only
        #########################################
        #cmds.file(force=True, save=True, type=ext)

    def get_tasks(self):
        return self.tasks

    def launch_task(self, task_no):
        if task_no < len(self.tasks):
            # send any existing camera in CAMERAS group to exported group
            cams = cmds.listRelatives("|{}|CAMERAS".format(self.exp_group), children=True)
            if not self.exported_cam_group:
                self.exported_cam_group = get_uuid(cmds.group(empty=True, name="Exported_Cameras_GP", world=True))
            if not self.not_exported:
                self.not_exported = get_uuid(cmds.group(empty=True, name="not_exported", world=True))
            if cams:
                for cam in cams:
                    # move this camera node into to export group Camera
                    cmds.parent(cam, get_uuid(self.exported_cam_group))

            camera = self.tasks[task_no][1]

            # find camera's top parent node
            # which will be - the node above "attach_ctrlOffset".
            # this is more specific to the current rig than I'd like, but it works for now.
            cam_dagpath = cmds.ls(camera, long=True)
            token = cam_dagpath[0].split('|')
            top_index = token.index("attach_ctrlOffset")
            cam_top_node = "|".join(token[:top_index])

            # full refresh will update the shot specific anim cheats
            shot_manager.full_refresh(camera)
            path = self.tasks[task_no][4]

            # set the group name to shot name
            if cmds.objExists("|{}".format(self.tasks[task_no][0])):
                cmds.rename("|{}".format(self.tasks[task_no][0]), "|{}_clash".format(self.tasks[task_no][0]))
            cmds.rename(self.exp_group, self.tasks[task_no][0])

            # move camera (with top parent node) under this group
            cmds.parent(cam_top_node, "|{}|CAMERAS".format(self.exp_group))

            # move nodes that are specifically hidden in the shot out of the export group
            full_cam_path = cmds.ls(camera, long=True)[0]
            for group in cmds.listRelatives(self.exp_group, children=True, fullPath=True):
                if group.endswith("CAMERAS"):
                    continue
                if cmds.objExists(group):
                    for this_node in cmds.listRelatives(group, children=True, fullPath=True):
                        vis = camera_membership.get_cameras_from_node(this_node)
                        if (full_cam_path in vis["hide"] or
                                (len(vis["vis"]) > 0 and full_cam_path not in vis["vis"].keys()) or
                                not find_visibility(this_node, self.tasks[task_no][2], self.tasks[task_no][3])):
                            not_exported_group = "{}|{}".format(get_uuid(self.not_exported), group.split("|")[-1])
                            if not cmds.objExists(not_exported_group):
                                cmds.group(em=True, name=group.split("|")[-1], parent=get_uuid(self.not_exported))
                            cmds.parent(this_node, not_exported_group)
            for group in cmds.listRelatives(get_uuid(self.not_exported), children=True, fullPath=True) or []:
                if group.endswith("CAMERAS"):
                    continue
                if cmds.objExists(path):
                    for this_node in cmds.listRelatives(group, children=True):
                        vis = camera_membership.get_cameras_from_node(this_node)
                        if (full_cam_path in vis["vis"] or
                                (len(vis["hide"]) > 0 and full_cam_path not in vis["hide"]) or
                                find_visibility(this_node, self.tasks[task_no][2], self.tasks[task_no][3])):
                            cmds.parent(this_node, "|{}|{}".format(self.exp_group, group.split("|")[-1]))

            #  alembic export command
            command = "-frameRange " + self.tasks[task_no][2] + " " + self.tasks[task_no][3] + " " \
                                   "-attr LocksmithVersion -attr overrideEnabled -attr overrideVisibility -writeVisibility -dataFormat ogawa " \
                                   "-root " + self.exp_group + " " \
                                   "-file " + path
            # We renamed the camera to something unique to keep track of it, but we want the
            # rig to be consistent for DNEG, so rename it back to "camera" for the export.
            new_name = cmds.rename(camera, "camera")
            # set the export
            cmds.AbcExport(j=command)
            cmds.file(newFile=True, force=True)
            self.playblast_cache(path)
            # increment and launch the next task
            # which's export next shot from the same maya scene (if any)
            task_no += 1
            self.launch_task(task_no)

    def playblast_cache(self, abc_file):
        imported = cmds.AbcImport(abc_file)
        cam = [x for x in cmds.listCameras(perspective=True) if x != "persp"][0]
        cmds.setAttr(cam + ".rnd", True)
        playblast_path = os.path.join(os.path.dirname(abc_file), "check.mov")
        cmds.playblast(filename=playblast_path,
                       format="qt",
                       compression="H.264",
                       startTime=cmds.getAttr(imported + ".startFrame"),
                       endTime=cmds.getAttr(imported + ".endFrame"),
                       viewer=False,
                       showOrnaments=False,
                       width=1920,
                       height=1080,
                       percent=100)


def find_visibility(node, start, end):
    cmds.currentTime(start)
    if not cmds.connectionInfo(node + ".visibility", isDestination=True):
        return cmds.getAttr(node + ".visibility")
    else:
        if cmds.getAttr(node + ".visibility"):
            return True
        else:
            keys = cmds.keyframe(node, query=True, attribute="visibility", time=(start, end), valueChange=True)
            if keys and 1 in keys:
                return True
    return False

def get_uuid(node):
    return cmds.ls(node, uuid=True)[0]

if __name__ == "__main__":
    maya_file_to_open = sys.argv[1]
    ext = sys.argv[2]
    shots = sys.argv[3]
    export_root = sys.argv[4]
    cams_str = sys.argv[5]
    start_str = sys.argv[6]
    end_str = sys.argv[7]

    # create alembic exporter object
    exp = AlembicExport()
    # setup the scene and task data
    exp.do_prepare(maya_file_to_open, ext, shots, export_root, cams_str, start_str, end_str)
    if len(exp.get_tasks()) > 0:
        # if tasks exists, launch the first one
        exp.launch_task(0)
