import os
import datetime
import sgtk
import shutil

from .task_group import TaskGroup

class AlembicExport(object):

    Export_Folder = "//hal/WIP/PROJECTS/BOT/DEPARTMENTS/PREVIS/Alembic_Exports"

    def __init__(self, shots=None):
        self.task_group = []
        self.shots = shots
        self.temp_path = ""

    def prepare_alembic(self, data=None):
        if data:
            self.copy_files_and_prepare_dict(data)
        else:
            maya_file, ext = self.export_this_maya_file()
            if maya_file and ext:
                self.launch_process(maya_file)
            else:
                print "Something's wrong. Please pass data dictionary if called outside maya env."

    def copy_files_and_prepare_dict(self, data):
        export_root = self.create_hal_folder()
        self.temp_path = self.create_temp_alembic_folder()
        dict = {}
        for key, val in data.iteritems():
            # copy maya file for further use
            # copy camera file as part of process
            filename = os.path.basename(key)
            maya_file, ext = self.get_maya_file_path(filename, self.temp_path)

            # copy maya file
            if not os.path.exists(maya_file):
                sgtk.util.filesystem.copy_file(key, maya_file)
            # get all cameras and copy them
            # prepare a new dict with maya files and shots, cams, start/end frames and ext
            shots = []
            cams = []
            start_frames = []
            end_frames = []
            for item in val:
                cam_name = self.check_camera_name(item[2])
                if cam_name:
                    cams.append(cam_name)
                else:
                    print "Camera name %s not valid. Should be in order of seq_camName_Camera" % item[3]
                    continue
                shot_folder_path = self.create_shot_folder(export_root, item[0])
                cam_path = self.get_camera_path(item[1], key)
                if cam_path:
                    copy_path = "{}\\{}".format(shot_folder_path, os.path.basename(item[1]))
                    sgtk.util.filesystem.copy_file(cam_path, copy_path)
                else:
                    print "No camera path found to copy.."

                shots.append(item[0])
                start_frames.append(item[3])
                end_frames.append(item[4])

            if (len(shots) != len(cams)) and (len(cams) != len(start_frames)) and (len(start_frames) != len(end_frames)):
                print "Mismatch between number of shots/cameras and/or its start and end frames for scene: " + maya_file
                continue

            if len(cams) > 0:
                temp_dict = {}
                temp_dict["ext"] = ext
                temp_dict["shots"] = shots
                temp_dict["cams"] = cams
                temp_dict["start"] = start_frames
                temp_dict["end"] = end_frames

                dict[maya_file] = temp_dict

        # launch task group
        tkg = TaskGroup()
        tkg.set_data(dict, export_root)
        self.task_group.append(tkg)

    def export_this_maya_file(self):
        maya_file = ""
        ext = ""
        try:
            import maya.cmds as cmds
        except ImportError:
            # called outside maya env - copy the file
            return maya_file, ext

        root_path = self.create_temp_alembic_folder()
        # get the file name and extension
        filepath = cmds.file(q=True, sn=True)
        filename = os.path.basename(filepath.encode('utf8'))
        maya_file, ext = self.get_maya_file_path(filename, root_path)

        print ("Exporting file %s..." % maya_file)
        cmds.file(maya_file,
                  force=True,
                  options="v=1",
                  type=ext,
                  pr=True,
                  ea=True)

        return maya_file, ext

    def get_maya_file_path(self, filename, root_path):
        raw_name, extension = os.path.splitext(filename)
        if len(raw_name.split('.')) < 2:
            raw_name = raw_name + ".v001"
        ext = extension.split('.')[1]
        maya_file = "{}/{}.{}".format(root_path, raw_name, ext)

        # get/save file with right version
        maya_file = self.find_version_path(maya_file, root_path, raw_name, ext)

        if ext == 'ma':
            ext = "mayaAscii"
        else:
            ext = "mayaBinary"

        return maya_file, ext

    def create_hal_folder(self):
        now = datetime.datetime.now()
        time_str = "{}{:02}{:02}{:02}{:02}".format(str(now.year)[-2:], now.month, now.day, now.hour, now.minute)
        export_folder_root = os.path.join(self.Export_Folder, "AlembicExport_" + time_str)
        if not os.path.exists(export_folder_root):
            os.makedirs(export_folder_root)

        return export_folder_root

    def create_shot_folder(self, path, name):
        shot_path = os.path.join(path, name)
        if not os.path.exists(shot_path):
            os.makedirs(shot_path)

        return shot_path

    def create_temp_alembic_folder(self):
        import tempfile

        root_path = tempfile.gettempdir()
        temp_dir = '%s/alembic' % root_path
        if not os.path.exists(temp_dir):
            os.makedirs(temp_dir)
        return temp_dir

    def find_version_path(self, maya_file, root_path, raw_name, ext):
        path_split = raw_name.split('.')
        if os.path.exists(maya_file) and (len(path_split) > 1) and ('v' in path_split[1]):
            version_str = path_split[1][1:len(path_split[1])]
            publish_clash = True
            while publish_clash:
                while os.path.exists(maya_file):
                    publish_clash = True
                    version_int = int("%03d" % int(version_str))
                    version_int += 1
                    version_str = ("%03d" % version_int)
                    maya_file = "{}/{}.v{}.{}".format(root_path, path_split[0], version_str, ext)

                publish_clash = False

        return maya_file

    def get_camera_path(self, cam_path, scene_path):
        scene_path_splt = scene_path.split('\\')
        cam_splt = cam_path.split('/')
        cam_bot_path = ""
        if len(cam_splt) > 0 and len(scene_path_splt) > 0:
            if cam_splt[0] == "bot":
                for scene_token in scene_path_splt:
                    if scene_token == "bot":
                        break
                    cam_bot_path += scene_token + "\\"
                if cam_bot_path:
                    path = ""
                    for cam_token in cam_splt:
                        path += cam_token + "\\"
                    cam_bot_path += path[:len(path) - 1]
        else:
            print "Couldn't split camera path or root path. Camera path : %s scene path: %s " % (cam_path, scene_path)

        if not cam_bot_path:
            cam_bot_path = cam_path

        return cam_bot_path

    def check_camera_name(self, cam):
        name = None
        splt = cam.split('_')
        if len(splt) > 2:
            name = cam[cam.find('_')+1:]

        return name

    def delete_files(self):
        if self.temp_path:
            for the_file in os.listdir(self.temp_path):
                file_path = os.path.join(self.temp_path, the_file)
                try:
                    if os.path.isfile(file_path):
                        print "Deleting file %s" % file_path
                        os.unlink(file_path)
                except Exception:
                    pass
