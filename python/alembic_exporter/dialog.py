import os
import operator

from tank.platform.qt import QtCore, QtGui

from .ui.dialog_widget import Ui_AlembicExporterDialog

class AppDialog(QtGui.QWidget):

    okpress = QtCore.Signal(dict)
    dialog_close = QtCore.Signal()

    def __init__(self, alembic_exporter):
        QtGui.QWidget.__init__(self)

        self.ui = Ui_AlembicExporterDialog()
        self.ui.setupUi(self)

        self.alembic_exporter = alembic_exporter
        self.app = alembic_exporter.app

        self.alembic_exporter.setup_ui(self)

        self.ui.shots_list_view.currentChanged = self.selection_changed

        self.cut_type = "Editorial"
        self.populate_values()

    def selection_changed(self, next, previous):
        cut_item = self.shots_list_view_model.item(next.row())
        print cut_item.data()

    def populate_values(self):
        project = self.app.context.project

        self.shots_list_view_model = QtGui.QStandardItemModel()
        self.ui.shots_list_view.setModel(self.shots_list_view_model)

        self.action_cache = self.app.shotgun.find("Scene", [["project", "is", project]], ["code", "sg_sequence"])
        self.cut_cache = self.app.shotgun.find("Cut",
                                               [["project", "is", project]],
                                               ["revision_number", "code", "description",
                                                "entity", "created_at", "sg_type"])

        self.sequences = self.app.shotgun.find("Sequence", [["project", "is", project]], ["code", "image"])
        self.sequences = sorted(self.sequences, key=operator.itemgetter("code"))
        for sequence in self.sequences:
            self.ui.seq_cmb.addItem(sequence["code"], sequence)
        self.on_sequence_change()

        self.ui.seq_cmb.currentIndexChanged.connect(self.on_sequence_change)
        self.ui.cut_cmb.currentIndexChanged.connect(self.on_load_cut)
        self.ui.ok_btn.clicked.connect(self.on_press_ok)

    def on_sequence_change(self):
        self.shots_list_view_model.clear()
        self.ui.cut_cmb.clear()
        sequence = self.ui.seq_cmb.itemData(self.ui.seq_cmb.currentIndex())
        actions = [x for x in self.action_cache if x["sg_sequence"] and x["sg_sequence"]["id"] == sequence["id"]]

        entites = []
        entites.append(sequence)
        for action in actions:
            entites.append(action)

        cuts = []
        for entity in entites:
            if entity:
                cuts.extend([x for x in self.cut_cache if (x["entity"] and x["entity"]["id"] == entity["id"]) and
                                                          (x["sg_type"] and x["sg_type"] == self.cut_type)])

        cuts = sorted(cuts, key=operator.itemgetter("created_at"), reverse=True)

        self.ui.cut_cmb.addItem("----", {})
        for cut in cuts:
            cut_text = "{} - {}".format(cut["code"], cut["description"])
            self.ui.cut_cmb.addItem(cut_text, cut)

    def on_load_cut(self, index):
        if index == -1 or not self.ui.cut_cmb.itemData(index):
            return

        self.shots_list_view_model.removeRows(0, self.shots_list_view_model.rowCount())
        cut = self.ui.cut_cmb.itemData(index)

        '''
        edit_data = CutData.from_shotgun(cut, self.app.shotgun)
        for shot in edit_data.shots:
            item = QtGui.QStandardItem(shot.name)
            item.setData(shot)
            self.shots_list_view_model.appendRow(item)
            #print shot.take.shotgun_entity
            #if shot.take.shotgun_entity["type"] == "CutItem":
            #    print "yes cut item"
        '''

        cut_items = self.app.shotgun.find('CutItem',
                                          [['cut', 'is', cut]],
                                          ['shot', 'code', 'sg_original_media', 'cut_item_in', 'cut_item_out'])

        for cut_item in cut_items:
            if cut_item["shot"]:
                name = cut_item["shot"]["name"]
                item = QtGui.QStandardItem(name)
                item.setData(cut_item)
                self.shots_list_view_model.appendRow(item)
                #items = self.app.shotgun.find("Version", [["id", "is", cut_item["sg_original_media"]["id"]]], ["sg_path_to_geometry"])

    def on_press_ok(self):
        selected_indexes = self.ui.shots_list_view.selectedIndexes()
        print selected_indexes
        if len(selected_indexes) > 0:
            msg = "Are you sure you want to export %s item(s) ?" % str(len(selected_indexes))
            reply = QtGui.QMessageBox.information(self, 'Info',
                                                  msg, QtGui.QMessageBox.Yes | QtGui.QMessageBox.No,
                                                  QtGui.QMessageBox.No)
            if reply == QtGui.QMessageBox.Yes:
                cut_items = []
                for index in selected_indexes:
                    cut_item = self.shots_list_view_model.item(index.row())
                    print cut_item
                    cut_items.append(cut_item.data())
                    print cut_item.data()
                if len(cut_items) > 0:
                    dict = self.get_maya_file_shots_camera(cut_items)
                    print dict
                    if dict:
                        self.okpress.emit(dict)
        else:
            msg = "Select shot(s) from the list to export alembic and camera..."
            QtGui.QMessageBox.critical(self, 'Error',
                                       msg, QtGui.QMessageBox.Ok)

    def get_maya_file_shots_camera(self, cut_items):
        dict = {}

        version_ids = []
        for item in cut_items:
            version_ids.append(item['sg_original_media']['id'])
        versions = self.app.shotgun.find('Version',
                                         [['id', 'in', version_ids]],
                                         ['sg_path_to_geometry', 'published_files', 'sg_cut_items', 'code', 'sg_first_frame', 'sg_last_frame'])

        publish_file_ids = []
        for version in versions:
            publishes = version['published_files']
            for publish in publishes:
                if self.is_maya_file(publish['name']):
                    publish_file_ids.append(publish['id'])
        published_files = []
        if len(publish_file_ids) > 0:
            published_files = self.app.shotgun.find('PublishedFile',
                                                    [['id', 'in', publish_file_ids]],
                                                    ['path_cache', 'name'])
        for item in cut_items:
            maya = next(x for x in versions if x["id"] == item['sg_original_media']['id'])
            cam = next(x for x in published_files if x["id"] in [y["id"] for y in maya["published_files"]])
            temp = []
            temp.append(item["code"])
            temp.append(cam['path_cache'])
            temp.append(cam['name'])
            temp.append(item['cut_item_in'] - 24)
            temp.append(item['cut_item_out'] + 24)
            if maya['sg_path_to_geometry'] not in dict:
                shot_and_cam = []
                shot_and_cam.append(temp)
                dict[maya['sg_path_to_geometry']] = shot_and_cam
            else:
                dict[maya['sg_path_to_geometry']].append(temp)
        print dict
        return dict

    def is_maya_file(self, file_name):
        maya_file = False
        splt = file_name.split('.')
        if len(splt) > 0:
            if (splt[len(splt) - 1] == "ma") or (splt[len(splt) - 1] == "mb"):
                maya_file = True

        return maya_file

    def closeEvent(self, event):
        self.okpress.disconnect(self.alembic_exporter.do_export)
        self.alembic_exporter.app.end_action()

        # disconnect all the task group and tasks and stop timers
        self.dialog_close.emit()

        event.accept()