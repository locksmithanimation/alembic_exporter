import maya.standalone
maya.standalone.initialize("Python")
from maya import cmds

def playblast_alembic(abc_file):



if __name__ == "__main__":
    maya_file_to_open = sys.argv[1]
    ext = sys.argv[2]
    shots = sys.argv[3]
    export_root = sys.argv[4]
    cams_str = sys.argv[5]
    start_str = sys.argv[6]
    end_str = sys.argv[7]

    # create alembic exporter object
    exp = AlembicExport()
    # setup the scene and task data
    exp.do_prepare(maya_file_to_open, ext, shots, export_root, cams_str, start_str, end_str)
    if len(exp.get_tasks()) > 0:
        # if tasks exists, launch the first one
        exp.launch_task(0)
