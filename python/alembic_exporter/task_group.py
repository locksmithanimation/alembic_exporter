import os

from .task import Task

from tank.platform.qt import QtCore, QtGui

class TaskGroup(QtCore.QObject):
    def __init__(self):
        QtCore.QObject.__init__(self)

        self.task_objects = []
        self.counter = 0

    def set_data(self, dict, export_root):
        # before setting up first task, create a list of all the tasks under this group
        self.create_tasks_list(dict, export_root)

        # once we have list of tasks, set one by one
        if self.counter < len(self.task_objects):
            self.set_new_task(self.counter)

    def create_tasks_list(self, metadata, export_root):
        for maya_file, shot_data in metadata.iteritems():
            task = Task(self, len(self.task_objects))
            shots = shot_data["shots"]
            cams = shot_data["cams"]
            start_frames = shot_data["start"]
            end_frames = shot_data["end"]
            ext = shot_data["ext"]
            task.create_tasks(maya_file, export_root, shots, cams, start_frames, end_frames, ext)
            self.task_objects.append(task)
            task.task_completed_signal.connect(self.check_tasks_status)

    def set_new_task(self, index):
        self.task_objects[index].start_timer()

    def check_tasks_status(self):
        # check if the task is over
        # if over, set next task
        # if all tasks over, proceed to rv thing
        task_done = True
        for task in self.task_objects:
            if task.get_status() == "Waiting":
                task_done = False
                self.counter += 1
                if self.counter < len(self.task_objects):
                    self.set_new_task(self.counter)
                break

        if task_done:
            msg = ""
            for task in self.task_objects:
                msg += task.get_shots_string()
                msg += ','

            print "--------------------------------------------------------------------"
            print "--------------------------------------------------------------------"
            print "---- Done: Alembic exported for all shots %s ----" % msg
            print "--------------------------------------------------------------------"
            print "--------------------------------------------------------------------"
