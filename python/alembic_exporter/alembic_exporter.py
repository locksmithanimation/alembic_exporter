from .prepare_tasks import AlembicExport

class Alembic_Exporter(object):
    def __init__(self, app):
        self.app = app
        self.prepare = AlembicExport()

    def show_alembic_exporter_dlg(self):
        """
        Runs Action Builder UI
        """

        from .dialog import AppDialog

        self.app.engine.show_dialog("Alembic Exporter", self.app, AppDialog, self)

    def setup_ui(self, dialog):
        dialog.okpress.connect(self.do_export)
        dialog.dialog_close.connect(self.main_dialog_close)

    def do_export(self, data):
        self.prepare.prepare_alembic(data)

    def main_dialog_close(self):
        self.prepare.delete_files()