import os
import sys
import subprocess
import tempfile

from tank.platform.qt import QtCore, QtGui

class Task(QtCore.QObject):

    task_completed_signal = QtCore.Signal()
    maya_bin = "C:\\Program Files\\Autodesk\\Maya2018\\bin"

    def __init__(self, parent, task_id):
        super(Task, self).__init__(parent)
        #QtCore.QObject.__init__(self)

        self.task_id = task_id
        self.status = "Waiting"
        self.maya_file = ""
        self.alembic_status = []
        self.timer = QtCore.QTimer(self)
        self.maya_cmd = ""
        self.proc_id = None
        self.maya_file = ""
        self.shot_string = ""

    def create_tasks(self, maya_file, export_root, shots, cams, start_frames, end_frames, ext):
        self.maya_file = maya_file
        for shot in shots:
            self.shot_string += (shot + ',')
            self.alembic_status.append(os.path.join(export_root, shot, shot + ".abc"))
        self.shot_string = self.shot_string[:len(self.shot_string) - 1]

        cam_string = self.list_to_string(cams)
        start_string = self.list_to_string(start_frames)
        end_string = self.list_to_string(end_frames)

        print "shots: " + self.shot_string
        print "ext: " + ext
        print "exp root: " + export_root
        print "cams: " + cam_string
        print "start: " + start_string
        print "end: " + end_string

        self.maya_cmd = '"{mayapy_exe}" "{maya_process}" "{open_maya_file}" "{ext}" "{shots}" "{export_path}" "{cams}" "{start_str}" "{end_str}"'. \
            format(mayapy_exe=(self.maya_bin + "\\mayapy"),
                   maya_process=os.path.dirname(os.path.realpath(__file__)) + "\\maya_call.py",
                   open_maya_file=self.maya_file,
                   ext=ext,
                   shots=self.shot_string,
                   export_path=export_root,
                   cams=cam_string,
                   start_str=start_string,
                   end_str=end_string
                   )

    def list_to_string(self, data):
        list_str = ""
        for token in data:
            list_str += (str(token) + ',')

        list_str = list_str[:len(list_str) - 1]
        return list_str

    def start_timer(self):
        self.timer.timeout.connect(self.timer_connect)
        self.launch_task()
        self.timer.start(5000)

    def launch_task(self):
        if self.maya_cmd:
            fd, batch_path = tempfile.mkstemp(".bat")

            with open(batch_path, 'w') as f:
                f.write(self.maya_cmd)
                f.close()

            proc = subprocess.Popen(batch_path, cwd=os.path.dirname(os.path.realpath(__file__)), shell=True)
            self.proc_id = proc.pid
            os.close(fd)

    def timer_connect(self):
        proc_exists = True
        alembic_exists = False
        out = subprocess.check_output(["tasklist", "/fi", "PID eq {pid}".format(pid=self.proc_id)], shell=True).strip()
        if out == "INFO: No tasks are running which match the specified criteria.":
            # check if alembic exists
            for alembic in self.alembic_status:
                if not os.path.exists(alembic):
                    alembic_exists = False
                    break
                else:
                    alembic_exists = True

            if alembic_exists:
                proc_exists = False
                print "----------- Alembic exported for shot %s --------------------- " % self.shot_string
        #else:
        #    print "Exporting shots from scene... " + self.maya_file

        if not proc_exists:
            # delete maya scene
            if os.path.exists(self.maya_file):
                os.remove(self.maya_file)

            self.timer.stop()
            self.status = "Completed"
            self.task_completed_signal.emit()

    def get_status(self):
        return self.status

    def get_shots_string(self):
        return self.shot_string