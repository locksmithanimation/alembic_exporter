import os
import subprocess

working_dir = os.path.dirname(os.path.abspath(__file__))
ui_files = [x for x in os.listdir(working_dir) if not os.path.isdir(os.path.join(working_dir, x)) and "." in x and x.split(".")[-1] == "ui"]

for ui_file in ui_files:
    ui_path = os.path.join(working_dir, ui_file)
    py_path = os.path.dirname(working_dir)
    py_path = os.path.join(py_path, "python")
    py_path = os.path.join(py_path, [x for x in os.listdir(py_path) if os.path.isdir(os.path.join(py_path, x))][0])
    py_path = os.path.join(py_path, "ui")
    py_path = os.path.join(py_path, ui_file.replace(".ui", ".py"))
    
    command = "C:\python27\scripts\pyside-uic -o " + py_path + " " + ui_path
    print command
    subprocess.call(command)
    
    data = None
    
    with open(py_path) as f:
        data = f.read()
    f.closed
    
    data = data.replace("from PySide", "from tank.platform.qt")
    
    with open(py_path, "w") as f:
        f.write(data)
    f.closed
    
    #from tank.platform.qt import QtCore, QtGui
