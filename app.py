# Copyright (c) 2013 Shotgun Software Inc.
# 
# CONFIDENTIAL AND PROPRIETARY
# 
# This work is provided "AS IS" and subject to the Shotgun Pipeline Toolkit 
# Source Code License included in this distribution package. See LICENSE.
# By accessing, using, copying or modifying this work you indicate your 
# agreement to the Shotgun Pipeline Toolkit Source Code License. All rights 
# not expressly granted therein are reserved by Shotgun Software Inc.

"""
An app that exports maya scene as alembic and saves camera file separately.

"""

from tank.platform.qt import QtCore, QtGui
import tank


class Alembic_Exporter(tank.platform.Application):

    def init_app(self):
        """
        App entry point
        """
        self.module = self.import_module("alembic_exporter")
        self.engine.register_command("alembic_exporter", self.show_alembic_exporter_dlg)

    @property
    def context_change_allowed(self):
        """
        Specifies that context changes are allowed.
        """
        return True

    def destroy_app(self):
        """
        App teardown
        """
        self.log_debug("Destroying Template")
    
    def show_alembic_exporter_dlg(self):
        """
        Shows the exporter Dialog.
        """
        self.alembic_exporter = self.module.Alembic_Exporter(self)
        return self.alembic_exporter.show_alembic_exporter_dlg()

    def end_action(self):
        self.alembic_exporter = None
